package org.niolasdev.revoluttest.storage

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import org.niolasdev.revoluttest.di.DaggerInjectionController
import org.niolasdev.revoluttest.di.component.AppComponent
import org.niolasdev.revoluttest.dto.CurrencyValue
import javax.inject.Inject

class SharedPrefsManager {

    @Inject
    internal lateinit var context: Context

    private val sharedPreferences: SharedPreferences

    init {
        DaggerInjectionController.getComponent(AppComponent::class.java).inject(this)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun saveCurrencyBase(value: CurrencyValue) {
        sharedPreferences.edit()
                .putString(KEY_BASE_CURRENCY_CODE, value.code)
                .putFloat(KEY_BASE_CURRENCY_AMOUNT, value.amount.toFloat())
                .apply()
    }

    fun loadCurrencyBase(): CurrencyValue {
        val code = sharedPreferences.getString(KEY_BASE_CURRENCY_CODE, "EUR")
        val amount = sharedPreferences.getFloat(KEY_BASE_CURRENCY_AMOUNT, 1.0f)
        return CurrencyValue(code, amount.toDouble())
    }

    companion object {
        const val KEY_BASE_CURRENCY_CODE = "BCV"
        const val KEY_BASE_CURRENCY_AMOUNT = "BCA"
    }

}