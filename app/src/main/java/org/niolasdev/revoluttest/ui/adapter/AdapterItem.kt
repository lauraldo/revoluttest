package org.niolasdev.revoluttest.ui.adapter

class AdapterItem(
        val currencyCode: String,
        val currencyAmount: Double,
        val position: Int
)