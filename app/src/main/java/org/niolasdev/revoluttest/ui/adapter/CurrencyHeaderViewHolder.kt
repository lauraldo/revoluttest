package org.niolasdev.revoluttest.ui.adapter

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.currency_list_item.view.*
import org.niolasdev.revoluttest.R
import org.niolasdev.revoluttest.dto.CurrencyValue

class CurrencyHeaderViewHolder private constructor(override val view: View) : CurrencyViewHolder(view) {

    private constructor(itemView: View, editListener: (Double) -> Unit) : this(itemView) {
        itemView.setBackgroundResource(R.color.skyBlue)
        itemView.amount_input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(amountStr: Editable?) {
                val d = amountStr?.toString()?.toDoubleOrNull()
                editListener(d ?: 0.0)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //Do nothing
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //Do nothing
            }
        })
    }

    override fun bind(item: CurrencyValue) {
        super.bind(item)
        itemView.amount_input.isEnabled = true
    }

    companion object {

        @JvmStatic
        fun create(parent: ViewGroup,
                   inflater: LayoutInflater,
                   editListener: (Double) -> Unit): CurrencyHeaderViewHolder {
            val itemView = inflater.inflate(R.layout.currency_list_item, parent, false)
            return CurrencyHeaderViewHolder(itemView, editListener)
        }
    }

}