package org.niolasdev.revoluttest.ui.presenter

import android.os.Build
import dagger.Provides
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import org.niolasdev.revoluttest.core.presenter.CorePresenter
import org.niolasdev.revoluttest.di.DaggerInjectionController
import org.niolasdev.revoluttest.dto.CurrencySnapshot
import org.niolasdev.revoluttest.dto.CurrencyValue
import org.niolasdev.revoluttest.ui.adapter.AdapterItem
import org.niolasdev.revoluttest.ui.model.CurrencyModel
import org.niolasdev.revoluttest.ui.view.CurrencyView
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

class CurrencyListPresenter : CorePresenter<CurrencyView>() {

    companion object {
        const val REFRESH_TIME_MS = 1000L
    }

    @Inject
    internal lateinit var model: CurrencyModel

    private val dataSubscription = CompositeDisposable()

    init {
        DaggerInjectionController.getComponent(Component::class.java, Module()).inject(this)
    }

    private fun getRefreshMetronome(msec: Long): Flowable<Long> {
        return Flowable.interval(msec, TimeUnit.MILLISECONDS)
    }

    //region ================================== CorePresenter ==================================

    override fun initView() {
        view?.let { view ->
            model.getStoredCurrencyBase()
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe { result: CurrencyValue ->
                        view.initData(result)
                    }
            dataSubscription += getRefreshMetronome(REFRESH_TIME_MS).withLatestFrom(
                    Flowable.combineLatest(view.getBaseAmountChanges(),
                            view.getBaseItemChanges().doOnNext {
                                view.updateBaseItem(it)
                            }, BiFunction { newAmount: Double, adapterItem: AdapterItem
                        -> CurrencyValue(adapterItem.currencyCode, newAmount) }
                    ), BiFunction { _: Long, newCurrencyValue: CurrencyValue ->
                            model.storeCurrencyBase(newCurrencyValue)
                            return@BiFunction newCurrencyValue
                        }
            )
            .flatMapSingle {
                model.getCurrencyRates(it.code, it.amount)
                        .doOnSuccess { snapshot ->
                            snapshot.baseAmount = it.amount
                        }.onErrorReturn { _ ->
                            CurrencySnapshot("", -1.0, mapOf())
                        }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { result ->
                result?.let {
                    if (result.baseAmount < 0) {
                        view.showError()
                    } else {
                        val newData = mutableListOf<CurrencyValue>()
                        newData.clear()
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            it.rates.forEach { code, amount ->
                                newData.add(CurrencyValue(code, amount))
                            }
                        } else {
                            for (entry in it.rates.entries) {
                                newData.add(CurrencyValue(entry.key, entry.value))
                            }
                        }
                        newData.add(0, CurrencyValue(result.base, result.baseAmount))
                        view.updateData(newData)
                    }
                }
            }
        }
    }

    override fun detachView() {
        dataSubscription.clear()
        super.detachView()
    }

    //endregion

    //region ================================== DI ==================================

    @dagger.Module
    inner class Module {

        @Provides
        @Singleton
        internal fun provideCurrencyModel(): CurrencyModel {
            return CurrencyModel()
        }
    }

    @dagger.Component(modules = [Module::class])
    @Singleton
    interface Component {
        fun inject(presenter: CurrencyListPresenter)
    }

    //endregion
}
