package org.niolasdev.revoluttest.ui.adapter

import android.icu.util.Currency
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.currency_list_item.view.*
import org.niolasdev.revoluttest.dto.CurrencyValue

abstract class CurrencyViewHolder internal constructor(open val view: View): RecyclerView.ViewHolder(view) {

    protected lateinit var item: CurrencyValue

    open fun bind(item: CurrencyValue) {
        this.item = item
        with(itemView) {
            Glide.with(context)
                    .load(getFlagUrlByCurrencyCode(item.code))
                    .into(currency_icon)
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ->
                    currency_description.text = Currency.getInstance(item.code).displayName
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ->
                    currency_description.text = java.util.Currency.getInstance(item.code).displayName
                else ->
                    currency_description.text = item.code
            }
            currency_code.text = item.code
            amount_input.setText(item.amount.format(2))
        }
    }

    companion object {

        @JvmStatic
        private fun getFlagUrlByCurrencyCode(code: String): String {
            val countryCode = code.dropLast(1)
            return "https://www.countryflags.io/$countryCode/flat/64.png"
        }
    }

}