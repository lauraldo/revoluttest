package org.niolasdev.revoluttest.ui.model

import dagger.Provides
import io.reactivex.Single
import org.niolasdev.revoluttest.core.provider.CurrencyBasePrefsStorageManager
import org.niolasdev.revoluttest.core.provider.CurrencyBaseStorageManager
import org.niolasdev.revoluttest.core.provider.RateProvider
import org.niolasdev.revoluttest.core.provider.RevolutRateProvider
import org.niolasdev.revoluttest.di.DaggerInjectionController
import org.niolasdev.revoluttest.dto.CurrencySnapshot
import org.niolasdev.revoluttest.dto.CurrencyValue
import javax.inject.Inject
import javax.inject.Singleton

class CurrencyModel {

    @Inject
    internal lateinit var rateProvider: RateProvider

    @Inject
    internal lateinit var currencyBaseStorage: CurrencyBasePrefsStorageManager

    init {
        DaggerInjectionController.getComponent(Component::class.java, Module()).inject(this)
    }

    fun getCurrencyRates(base: String, amount: Double): Single<CurrencySnapshot> {
        return rateProvider.provideCurrencyRates(base, amount)
    }

    fun getStoredCurrencyBase() = currencyBaseStorage.getCurrencyBase()

    fun storeCurrencyBase(value: CurrencyValue) = currencyBaseStorage.setCurrencyBase(value)

    //region ================================== DI ==================================

    @dagger.Module
    inner class Module {

        @Provides
        @Singleton
        internal fun provideRateProvider(): RateProvider {
            return RevolutRateProvider()
        }

        @Provides
        @Singleton
        internal fun provideBaseStorageManager(): CurrencyBasePrefsStorageManager {
            return CurrencyBasePrefsStorageManager()
        }
    }

    @dagger.Component(modules = [Module::class])
    @Singleton
    interface Component {
        fun inject(model: CurrencyModel)
    }

    //endregion
}