package org.niolasdev.revoluttest.ui

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import dagger.Provides
import io.reactivex.Flowable
import kotlinx.android.synthetic.main.activity_main.*
import org.niolasdev.revoluttest.R
import org.niolasdev.revoluttest.core.view.BaseActivity
import org.niolasdev.revoluttest.di.DaggerInjectionController
import org.niolasdev.revoluttest.dto.CurrencyValue
import org.niolasdev.revoluttest.ui.adapter.AdapterItem
import org.niolasdev.revoluttest.ui.adapter.CurrencyListAdapter
import org.niolasdev.revoluttest.ui.presenter.CurrencyListPresenter
import org.niolasdev.revoluttest.ui.view.CurrencyView
import javax.inject.Inject
import javax.inject.Singleton

class MainActivity : BaseActivity(), CurrencyView {

    @Inject
    internal lateinit var presenter: CurrencyListPresenter

    //TODO show zeros when user enters zero or empty string in the top text input
    private lateinit var currencyListAdapter: CurrencyListAdapter

    //region ================================== BaseActivity ==================================

    override val rootView: View?
        get() = containerView

    //endregion

    //region ================================== CurrencyView ==================================

    override fun getBaseItemChanges(): Flowable<AdapterItem> {
        return currencyListAdapter.getItemClicks()
    }

    override fun getBaseAmountChanges(): Flowable<Double> {
        return currencyListAdapter.getItemAmountChanges()
    }

    override fun updateBaseItem(item: AdapterItem) {
        currencyListAdapter.raise(item)
        currencyList.scrollToPosition(0)
    }

    override fun updateData(data: MutableList<CurrencyValue>) {
        currencyListAdapter.resetData(data)
        if (currencyList.visibility != View.VISIBLE) {
            emptyView.visibility = View.GONE
            currencyList.visibility = View.VISIBLE
        }
    }

    override fun initData(value: CurrencyValue) = currencyListAdapter.initWithDefaultValue(value)

    override fun showError() {
        if (emptyView.visibility != View.VISIBLE) {
            currencyList.visibility = View.GONE
            emptyView.visibility = View.VISIBLE
            showMessage(getString(R.string.poll_error))
        }
    }

    //endregion

    //region ================================== Lifecycle ==================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DaggerInjectionController.getComponent(Component::class.java, Module()).inject(this)

        currencyList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false)
        currencyListAdapter = CurrencyListAdapter()
        currencyList.adapter = currencyListAdapter

        presenter.attachView(this)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }
    
    //endregion

    //region ================================== DI ==================================

    @dagger.Module
    inner class Module {

        @Provides
        @Singleton
        internal fun provideCurrencyPresenter(): CurrencyListPresenter {
            return CurrencyListPresenter()
        }
    }

    @dagger.Component(modules = [Module::class])
    @Singleton
    interface Component {
        fun inject(activity: MainActivity)
    }

    //endregion
}
