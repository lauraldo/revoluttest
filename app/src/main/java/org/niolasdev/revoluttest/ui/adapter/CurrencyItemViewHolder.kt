package org.niolasdev.revoluttest.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.currency_list_item.view.*
import org.niolasdev.revoluttest.R
import org.niolasdev.revoluttest.dto.CurrencyValue

class CurrencyItemViewHolder private constructor(override val view: View) : CurrencyViewHolder(view) {

    private constructor(itemView: View, clickListener: (AdapterItem) -> Unit) : this(itemView) {
        itemView.amount_input.isEnabled = false
        itemView.setOnClickListener {
            clickListener(AdapterItem(item.code, item.amount, adapterPosition))
        }
    }

    override fun bind(item: CurrencyValue) {
        super.bind(item)
        itemView.amount_input.isEnabled = false
    }

    companion object {

        @JvmStatic
        fun create(parent: ViewGroup,
                   inflater: LayoutInflater,
                   clickListener: (AdapterItem) -> Unit): CurrencyItemViewHolder {
            val itemView = inflater.inflate(R.layout.currency_list_item, parent, false)
            return CurrencyItemViewHolder(itemView, clickListener)
        }
    }
}