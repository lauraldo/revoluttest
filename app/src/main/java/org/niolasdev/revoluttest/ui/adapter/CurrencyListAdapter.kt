package org.niolasdev.revoluttest.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.PublishProcessor
import org.niolasdev.revoluttest.dto.CurrencyValue

fun Double.format(digits: Int): String? = java.lang.String.format("%.${digits}f", this)

class CurrencyListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<CurrencyValue> = mutableListOf()

    private val onItemClickProcessor: BehaviorProcessor<AdapterItem> = BehaviorProcessor.create()
    private val onAmountEditProcessor: BehaviorProcessor<Double> = BehaviorProcessor.create()

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) TYPE_HEADER else TYPE_ITEM
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == TYPE_HEADER) {
            CurrencyHeaderViewHolder.create(parent, inflater) {
                onAmountEditProcessor.onNext(it)
            }
        } else {
            CurrencyItemViewHolder.create(parent, inflater) {
                onItemClickProcessor.onNext(it)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CurrencyViewHolder) {
            holder.bind(items[position])
        } else {
            throw IllegalStateException("ViewHolder must implement BindingHolder interface.")
        }
    }

    fun resetData(data: MutableList<CurrencyValue>) {
        items = data
        notifyItemRangeChanged(1, items.size - 1)
    }

    fun getItemClicks(): Flowable<AdapterItem> {
        return onItemClickProcessor
    }

    fun getItemAmountChanges(): Flowable<Double> {
        return onAmountEditProcessor
    }

    fun raise(item: AdapterItem) {
        if (item.position != 0) {
            notifyItemMoved(item.position, 0)
            items[0] = CurrencyValue(item.currencyCode, item.currencyAmount)
            notifyItemChanged(0)
        }
    }

    fun initWithDefaultValue(value: CurrencyValue) {
        onItemClickProcessor.onNext(AdapterItem(value.code, value.amount, 0))
        onAmountEditProcessor.onNext(value.amount)
    }

    companion object {
        const val TYPE_HEADER = 0
        const val TYPE_ITEM = 1
    }
}