package org.niolasdev.revoluttest.ui.view

import io.reactivex.Flowable
import org.niolasdev.revoluttest.core.view.CoreView
import org.niolasdev.revoluttest.dto.CurrencyValue
import org.niolasdev.revoluttest.ui.adapter.AdapterItem

interface CurrencyView: CoreView {

    fun initData(value: CurrencyValue)

    fun getBaseItemChanges(): Flowable<AdapterItem>

    fun getBaseAmountChanges(): Flowable<Double>

    fun updateBaseItem(item: AdapterItem)

    fun updateData(data: MutableList<CurrencyValue>)

    fun showError()
}