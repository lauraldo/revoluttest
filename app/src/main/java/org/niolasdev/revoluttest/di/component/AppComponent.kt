package org.niolasdev.revoluttest.di.component

import android.content.Context
import dagger.Component
import org.niolasdev.revoluttest.di.module.AppModule
import org.niolasdev.revoluttest.storage.SharedPrefsManager
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun getContext(): Context

    fun inject(spm: SharedPrefsManager)

}