package org.niolasdev.revoluttest.di.component

import dagger.Component
import org.niolasdev.revoluttest.core.provider.CurrencyBasePrefsStorageManager
import org.niolasdev.revoluttest.core.provider.CurrencyBaseStorageManager
import org.niolasdev.revoluttest.core.provider.RevolutRateProvider
import org.niolasdev.revoluttest.di.module.NetworkModule
import org.niolasdev.revoluttest.di.module.StorageModule
import javax.inject.Singleton

@Component(modules = [NetworkModule::class, StorageModule::class])
@Singleton
interface DataAccessComponent {

    fun inject(provider: RevolutRateProvider)

    fun inject(storageManager: CurrencyBasePrefsStorageManager)

}