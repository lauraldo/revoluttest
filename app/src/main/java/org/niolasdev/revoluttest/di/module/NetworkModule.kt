package org.niolasdev.revoluttest.di.module

import dagger.Module
import dagger.Provides
import org.niolasdev.revoluttest.core.provider.RevolutRateProvider
import org.niolasdev.revoluttest.network.CurrencyApi
import org.niolasdev.revoluttest.network.UnsafeOkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    internal fun provideCurrencyApi(): CurrencyApi {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(UnsafeOkHttpClient.unsafeOkHttpClient)
                .baseUrl(RevolutRateProvider.BASE_URL)
                .build()
                .create(CurrencyApi::class.java)
    }

}