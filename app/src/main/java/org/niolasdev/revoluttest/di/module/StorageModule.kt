package org.niolasdev.revoluttest.di.module

import dagger.Module
import dagger.Provides
import org.niolasdev.revoluttest.storage.SharedPrefsManager
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    internal fun provideSharedPrefsManager()
            = SharedPrefsManager()
}