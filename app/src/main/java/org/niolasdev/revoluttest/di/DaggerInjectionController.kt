package org.niolasdev.revoluttest.di

import org.niolasdev.revoluttest.di.component.AppComponent

object DaggerInjectionController {

    @Volatile
    private var components = HashMap<Class<*>, Any>()

    fun initAppComponent(appComponent: AppComponent) {
        components[AppComponent::class.java] = appComponent
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> getComponent(componentClass: Class<T>, vararg dependencies: Any): T {
        var component: Any? = components[componentClass]

        if (component == null) {
            component = createComponent(componentClass, *dependencies)
            registerComponent(componentClass, component!!)
        }

        return component as T
    }

    /*
     * Kludged code
     * Use carefully, Dagger lib may change generated sources naming protocol
     */
    @Suppress("UNCHECKED_CAST")
    private fun <T> createComponent(componentClass: Class<T>, vararg dependencies: Any): T {

        val componentName = componentClass.name

        val packageName = componentClass.getPackage().name
        val simpleName = componentName.substring(packageName.length + 1)
        val generatedName = "$packageName.Dagger$simpleName".replace('$', '_')

        try {
            val generatedClass = Class.forName(generatedName)
            val builder = generatedClass.getMethod("builder").invoke(null)

            for (method in builder.javaClass.methods) {
                val params = method.parameterTypes
                if (params.size == 1) {
                    val dependencyClass = params[0]
                    for (dependency in dependencies) {
                        if (dependencyClass.isAssignableFrom(dependency.javaClass)) {
                            method.invoke(builder, dependency)
                            break
                        }
                    }
                }
            }

            return builder.javaClass.getMethod("build").invoke(builder) as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }

    private fun registerComponent(componentClass: Class<*>, daggerComponent: Any) {
        components[componentClass] = daggerComponent
    }

}
