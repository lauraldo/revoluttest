package org.niolasdev.revoluttest.network

import io.reactivex.Single
import org.niolasdev.revoluttest.dto.CurrencySnapshot
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {

    @GET("/latest")
    fun getLatest(@Query("base") baseCurrency: String, @Query("amount") amount: Double): Single<CurrencySnapshot>

}