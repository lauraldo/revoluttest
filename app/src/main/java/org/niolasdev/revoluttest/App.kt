package org.niolasdev.revoluttest

import android.app.Application

import org.niolasdev.revoluttest.di.DaggerInjectionController
import org.niolasdev.revoluttest.di.component.DaggerAppComponent
import org.niolasdev.revoluttest.di.module.AppModule

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger() {
        DaggerInjectionController.initAppComponent(DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build())
    }
}
