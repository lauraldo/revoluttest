package org.niolasdev.revoluttest.dto

data class CurrencySnapshot(
        val base: String,
        var baseAmount: Double,
        val rates: Map<String, Double>
)