package org.niolasdev.revoluttest.dto

data class CurrencyValue(
        val code: String, val amount: Double
)