package org.niolasdev.revoluttest.core.view

interface CoreView {

    fun showMessage(msg: String)
}