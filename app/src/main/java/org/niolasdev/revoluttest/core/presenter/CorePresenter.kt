package org.niolasdev.revoluttest.core.presenter

import org.niolasdev.revoluttest.core.view.CoreView

abstract class CorePresenter<V : CoreView> {

    var view: V? = null
        private set

    fun attachView(view: V) {
        this.view = view
        initView()
    }

    open fun detachView() {
        view = null
    }

    abstract fun initView()
}
