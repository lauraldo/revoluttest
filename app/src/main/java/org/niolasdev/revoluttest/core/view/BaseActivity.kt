package org.niolasdev.revoluttest.core.view

import android.annotation.SuppressLint
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View

@SuppressLint("Registered")
abstract class BaseActivity : AppCompatActivity(), CoreView {

    protected abstract val rootView: View?

    override fun showMessage(msg: String) {
        rootView?.let {
            Snackbar.make(it, msg, Snackbar.LENGTH_LONG).show()
        }
    }
}
