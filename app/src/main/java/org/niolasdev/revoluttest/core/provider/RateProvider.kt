package org.niolasdev.revoluttest.core.provider

import io.reactivex.Single
import org.niolasdev.revoluttest.dto.CurrencySnapshot

interface RateProvider {

    fun provideCurrencyRates(base: String, amount: Double): Single<CurrencySnapshot>

}