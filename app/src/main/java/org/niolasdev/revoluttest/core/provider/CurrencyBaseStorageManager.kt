package org.niolasdev.revoluttest.core.provider

import io.reactivex.Single
import org.niolasdev.revoluttest.dto.CurrencyValue

interface CurrencyBaseStorageManager {

    fun getCurrencyBase(): Single<CurrencyValue>

    fun setCurrencyBase(value: CurrencyValue)

}