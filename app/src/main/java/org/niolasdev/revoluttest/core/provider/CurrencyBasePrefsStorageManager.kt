package org.niolasdev.revoluttest.core.provider

import io.reactivex.Single
import org.niolasdev.revoluttest.di.DaggerInjectionController
import org.niolasdev.revoluttest.di.component.DataAccessComponent
import org.niolasdev.revoluttest.dto.CurrencyValue
import org.niolasdev.revoluttest.storage.SharedPrefsManager
import javax.inject.Inject
class CurrencyBasePrefsStorageManager: CurrencyBaseStorageManager {

    @Inject
    lateinit var sharedPrefsManager: SharedPrefsManager

    private var buffer: CurrencyValue? = null

    init {
        DaggerInjectionController.getComponent(DataAccessComponent::class.java)
                .inject(this)
    }

    override fun getCurrencyBase(): Single<CurrencyValue> {
        buffer?.let {
            return Single.just(buffer)
        }
        return Single.just(sharedPrefsManager.loadCurrencyBase())
    }

    override fun setCurrencyBase(value: CurrencyValue) {
        buffer?.let {
            if (value == buffer) {
                return
            }
        }
        buffer = value
        sharedPrefsManager.saveCurrencyBase(value)
    }

}

