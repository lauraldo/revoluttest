package org.niolasdev.revoluttest.core.provider

import io.reactivex.Single
import org.niolasdev.revoluttest.di.DaggerInjectionController
import org.niolasdev.revoluttest.di.component.DataAccessComponent
import org.niolasdev.revoluttest.di.module.NetworkModule
import org.niolasdev.revoluttest.dto.CurrencySnapshot
import org.niolasdev.revoluttest.network.CurrencyApi
import javax.inject.Inject

class RevolutRateProvider: RateProvider {

    @Inject
    internal lateinit var remoteApi: CurrencyApi

    init {
        DaggerInjectionController.getComponent(DataAccessComponent::class.java, NetworkModule())
                .inject(this)
    }

    //region ================================== RateProvider ==================================

    override fun provideCurrencyRates(base: String, amount: Double): Single<CurrencySnapshot> {
        return remoteApi.getLatest(base, amount)
    }

    //endregion

    companion object {
        const val BASE_URL = "https://revolut.duckdns.org"
    }
}